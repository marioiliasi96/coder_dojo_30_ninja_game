﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterController : MonoBehaviour {

    public float speed = 20;
    Animator animator;
    bool facingRight = true;
    // Use this for initialization
    void Start () {
        animator = GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {
        bool attack = Input.GetMouseButtonDown(0);
        if (attack == true)
        {
            animator.SetTrigger("attack");
        }
        float horizontal = Input.GetAxis("Horizontal");
        animator.SetFloat("speed", Mathf.Abs(horizontal));
        transform.Translate(speed * horizontal * Time.deltaTime, 0, 0);
        flip(horizontal);
        
	}

    void flip(float horizontal)
    {
        if( (horizontal > 0 && facingRight == false) || (horizontal < 0 && facingRight))
        {
            facingRight = !facingRight;
            SpriteRenderer spriteRenderer = GetComponent<SpriteRenderer>();
            spriteRenderer.flipX = !facingRight;
        }
    }
}
